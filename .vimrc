set pyxversion=3
" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')
Plug 'tikhomirov/vim-glsl'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'mileszs/ack.vim'
" Plug 'craigemery/vim-autotag'
Plug 'Yggdroot/indentLine'
Plug 'Glench/Vim-Jinja2-Syntax'
Plug 'roxma/nvim-yarp'
Plug 'roxma/vim-hug-neovim-rpc'
Plug 'Shougo/deoplete.nvim'
Plug 'deoplete-plugins/deoplete-jedi'
call plug#end()

filetype indent on

set encoding=utf-8
setglobal fileencoding=utf-8
set fileencodings=utf-8,latin1
set nu
set relativenumber
set tabstop=4
set shiftwidth=4
set expandtab
set smarttab
set wildmenu
set hlsearch
set showcmd
set ruler
set laststatus=2
set tags=tags;
set incsearch
set colorcolumn=120
set t_Co=256
set background=dark
set conceallevel=0

set wildignore+=*.pyc,*.so,*.class,*.a,*.o,*.zip,*.tar,*.gz,*.7z,*.swp,*.jpeg,*.jpg,*.png,*.bmp,*.exe,*.scssc

nnoremap ,h :find %:h<CR>
nnoremap <C-A> :Ack 
nnoremap <C-K> :CtrlPLine<CR>
nnoremap <C-T> :CtrlPTag<CR>
nnoremap <C-L> :CtrlPLine<CR>

if executable('ag')
    " Use Ag over Grep
    set grepprg=ag\ --nogroup\ --nocolor
    let g:ctrlp_extensions = ['line']

    " Use ag in CtrlP for listing files. Lightning fast and respects .gitignore
    let g:ctrlp_user_command = 'ag %s -l -g ""'
    let g:ctrlp_use_caching = 0
    let g:ackprg = 'ag --vimgrep'
endif

let g:deoplete#enable_at_startup = 1

" :highlight NeomakeError ctermfg=15 ctermbg=1 guifg=White guibg=Red
" :highlight NeomakeWarning ctermfg=0 ctermbg=11 guifg=Black guibg=Yellow
highlight ColorColumn ctermfg=14 ctermbg=242 guifg=Cyan guibg=Grey
